using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Google.Maps;

namespace MyGoogleMapsApp
{
	public partial class MyGoogleMapsAppViewController : UIViewController
	{

		MapView mapView;
		public MyGoogleMapsAppViewController (IntPtr handle) : base (handle)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		#region View lifecycle

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			// Perform any additional setup after loading the view, typically from a nib.
		}



		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
		}

		public override void ViewDidDisappear (bool animated)
		{
			base.ViewDidDisappear (animated);
		}

		public override void LoadView ()
		{
			base.LoadView ();

			var camera = CameraPosition.FromCamera (latitude: 39.150173, 
				longitude: -86.575119, 
				zoom:17,
				bearing:15,
				viewingAngle:15);
			mapView = MapView.FromCamera (View.Bounds, camera);
			mapView.MapType = MapViewType.Normal;
			mapView.MyLocationEnabled = true;
			View = mapView;
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			mapView.StartRendering ();
		}

		public override void ViewWillDisappear (bool animated)
		{   
			mapView.StopRendering ();
			base.ViewWillDisappear (animated);
		}

		#endregion

	}
}

